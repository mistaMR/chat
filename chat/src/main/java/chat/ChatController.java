package chat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller	
public class ChatController {
	
	private final ChatStorage chatStorage;
	
	@Autowired
	public ChatController(ChatStorage chatStorage) {
		this.chatStorage = chatStorage;
	}
	
	@MessageMapping("/user")
	@SendTo("/topic/user")
	public User greeting(User user) {
		return user;	
		
	}
	@MessageMapping("/chat")
	@SendTo("/topic/message")
	public Message sendMessage(Message message) {
		chatStorage.addMessage(message);
		return message;
	}
}
