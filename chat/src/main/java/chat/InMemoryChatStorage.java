package chat;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.springframework.stereotype.Repository;

@Repository
public class InMemoryChatStorage implements ChatStorage {
	
	private final List<Message> messages = new CopyOnWriteArrayList<Message>();

	public List<Message> getMessages() {
		return messages;
	}

	public void addMessage(Message message) {
		messages.add(message);		
	}
}
