package chat;

import java.util.List;

public interface ChatStorage {
	
	List<Message> getMessages();
	
	void addMessage(Message message);
}
