package chat;

public class Message {

	private String name;
	private String text;
		
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getMsg() {
		return text;
	}
	
	public void setMsg(String text) {
		this.text = text;
	}

	public String makeMessage() {
		return name + ": " + text;
	}
}
